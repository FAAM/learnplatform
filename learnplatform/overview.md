# Overview

## Description
Nelson Mandela believed education was the most powerful weapon to change the world. But not every student has equal opportunities to learn. Effective policies and plans need to be enacted in order to make education more equitable—and perhaps your innovative data analysis will help reveal the solution.

Current research shows educational outcomes are far from equitable. The imbalance was exacerbated by the COVID-19 pandemic. There's an urgent need to better understand and measure the scope and impact of the pandemic on these inequities.

Education technology company LearnPlatform was founded in 2014 with a mission to expand equitable access to education technology for all students and teachers. LearnPlatform’s comprehensive edtech effectiveness system is used by districts and states to continuously improve the safety, equity, and effectiveness of their educational technology. LearnPlatform does so by generating an evidence basis for what’s working and enacting it to benefit students, teachers, and budgets.

In this analytics competition, you’ll work to uncover trends in digital learning. Accomplish this with data analysis about how engagement with digital learning relates to factors like district demographics, broadband access, and state/national level policies and events. Then, submit a Kaggle Notebook to propose your best solution to these educational inequities.

Your submissions will inform policies and practices that close the digital divide. With a better understanding of digital learning trends, you may help reverse the long-term learning loss among America’s most vulnerable, making education more equitable.

### Problem Statement
The COVID-19 Pandemic has disrupted learning for more than 56 million students in the United States. In the Spring of 2020, most states and local governments across the U.S. closed educational institutions to stop the spread of the virus. In response, schools and teachers have attempted to reach students remotely through distance learning tools and digital platforms. Until today, concerns of the exacaberting digital divide and long-term learning loss among America’s most vulnerable learners continue to grow.

### Challenge
We challenge the Kaggle community to explore (1) the state of digital learning in 2020 and (2) how the engagement of digital learning relates to factors such as district demographics, broadband access, and state/national level policies and events.

We encourage you to guide the analysis with questions that are related to the themes that are described above (in bold font). Below are some examples of questions that relate to our problem statement:

* What is the picture of digital connectivity and engagement in 2020?
* What is the effect of the COVID-19 pandemic on online and distance learning, and how might this also evolve in the future?
* How does student engagement with different types of education technology change over the course of the pandemic?
* How does student engagement with online learning platforms relate to different geography? Demographic context (e.g., race/ethnicity, ESL, learning disability)? Learning context? Socioeconomic status?
* Do certain state interventions, practices or policies (e.g., stimulus, reopening, eviction moratorium) correlate with the increase or decrease online engagement?


## Evaluation
**What is the state of digital learning in 2020? And how does the engagement of digital learning relate to factors such as district demographics, broadband access, and state/national level policies and events?**

This is an Analytics competition where your task is to create a Notebook that best addresses the Evaluation criteria below. 
Submissions should be shared directly as specified under [Submission Instructions](https://www.kaggle.com/c/learnplatform-covid19-impact-on-digital-learning/overview/submission-instructions) with host and will be judged by the LearnPlatform team based on how well they address:

**Clarity (5 pts)**

Did the author present a clear thread of questions or themes motivating their analysis?
Did the author document why/what/how a set of methods was chosen and used for their analysis?
Is the notebook documented in a way that is easily reproducible (e.g., code, additional data sources, citations)?
Does the notebook contain clear data visualizations that help effectively communicate the author’s findings to both experts and non-experts?

**Accuracy (5 pts)**

Did the author process the data (e.g., merging) and/or additional data sources accurately?
Is the methodology used in the analysis appropriate and reasonable?
Are the interpretations based on the analysis and visualization reasonable and convincing?

**Creativity (5 pts)**

Does the notebook help the reader learn something new or challenge the reader to think in a new way?
Does the notebook leverage novel methods and/or visualizations that help reveal insights from data and/or communicate findings?
Did the author utilize additional public data sources in their analysis?
A more detailed problem statement and challenge description can be found on the [Description page](https://www.kaggle.com/c/learnplatform-covid19-impact-on-digital-learning/overview/description).


## Timeline

* August 2, 2021 - Start Date.
* September 30, 2021 - Entry and Final Submission Deadline. All Team Members must accept the competition rules and [make your submission](https://www.kaggle.com/c/learnplatform-covid19-impact-on-digital-learning/overview/submission-instructions) before this date in order to compete.
* October 28, 2021 - Winners Announced.

All deadlines are at 11:59 PM UTC on the corresponding day unless otherwise noted. The competition organizers reserve the right to update the contest timeline if they deem it necessary.

## Prizes
Five (5) Winners - $4,000 each


## Submission Instructions

You can make as many submissions as you like, 
but we will only consider your latest submission before the [submission deadline](https://www.kaggle.com/c/learnplatform-covid19-impact-on-digital-learning/overview/timeline
).

If you are submitting as a team, you do NOT need to merge within the Kaggle platform (it's actually disabled), but all team members must be listed as collaborators on the submitted Notebook, and all team members must accept the competition rules before the submission deadline.

A valid submission will include a Notebook Analysis. All notebooks submitted must be made public by the submission deadline to be eligible. If submitting as a team, all team members must be listed as collaborators on all notebooks submitted.

Click here to go to the [Submission form](https://www.kaggle.com/learnplatform-analytics)
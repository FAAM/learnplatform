# LearnPlatform COVID-19 Impact on Digital Learning


<a href="https://faam.gitlab.io/learnplatform/"><img alt="Link a la Documentación" src="https://jupyterbook.org/badge.svg"></a>
[![pipeline status](	https://img.shields.io/badge/Kaggle-link-20BEFF?style=flat-square&logo=kaggle&logoColor=white)](https://www.kaggle.com/faamds/basic-analysis-impact-on-digital-learning)
[![pipeline status](https://gitlab.com/FAAM/learnplatform/badges/master/pipeline.svg)](https://gitlab.com/FAAM/learnplatform/-/commits/master)

Use digital learning data to analyze the impact of COVID-19 on student learning
